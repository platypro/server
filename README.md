Platypro's Server Utilities
===========================

Tools and documentation for how to run, configure, and migrate, platypro.net.

- utilities/serve.sh - Serverside util script
- utilities/publish.sh - Clientside publish script

# Setting up server

1. You must first initialize. This is done by running `./serve.sh init`. This does a few things:

  - Initializes Docker Swarm
  - Generates `sql_root_pw.secret`
  - Generates `sql_nextcloud_pw.secret`

The `.secret` files store passwords for the root and nextcloud users respectively

2. You can either run `./serve.sh start` to start services, or `./serve.sh restore` to restore from backup.
